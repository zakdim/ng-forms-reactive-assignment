import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { CustomValidators } from './custom-validators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  statusOptions = [
    { value: 'stable', label: 'Stable'},
    { value: 'critical', label: 'Critical'},
    { value: 'finished', label: 'Finished'}
  ];
  projectForm: FormGroup;

  ngOnInit() {
    this.projectForm = new FormGroup({
      'projectName': new FormControl(
        null,
        [Validators.required, CustomValidators.invalidProjectName],
        CustomValidators.asyncInvalidProjectName
      ),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'projectStatus': new FormControl(this.statusOptions[0].value)
    });
  }

  onSubmit() {
    console.log(this.projectForm.value);
  }

  // invalidProjectName(control: FormControl): {[s: string]: boolean } {
  //   if (control.value === 'Test') {
  //     return {'invalidProjectName': true};
  //   }
  //   return null;
  // }

  // asyncInvalidProjectName(control: FormControl): Promise<any> | Observable<any> {
  //   const promise = new Promise<any>((resolve, reject) => {
  //     setTimeout(() => {
  //       if (control.value === 'Testproject') {
  //         resolve({'projectIsForbidden': true});
  //       } else {
  //         resolve(null);
  //       }
  //     }, 2000);
  //   });
  //   return promise;
  // }
}
